import peewee
from playhouse.db_url import connect

from . import config

db_url = getattr(config, "DATABASE_URL")
db = connect(db_url)

if db_url.startswith("sqlite"):
    db.pragma("foreign_keys", 1, permanent=True)
