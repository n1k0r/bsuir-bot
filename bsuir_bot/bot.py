import logging
import sys
import traceback
from pydoc import locate

import telebot

from . import config
from .database import db
from .models import BaseModel

CONFIG_OPTIONS = {
    "API_TOKEN": str,
    "DATABASE_URL": str,
    "BOT_NAME": str,
    "STUDENT_GROUP": str,
    "PRIVILEGED_USERS": (tuple, list),
    "BOOKS": (tuple, list),
    "HANDLERS": (tuple, list),
}
for option, option_type in CONFIG_OPTIONS.items():
    config_option = getattr(config, option, None)
    if config_option is None:
        sys.exit("{} is required".format(option))

    if not isinstance(config_option, option_type):
        if isinstance(option_type, tuple):
            exp_type_str = " or ".join([opt.__name__ for opt in option_type])
        else:
            exp_type_str = option_type.__name__

        sys.exit("{option} have to be of type {expected_type} not {current_type}".format(
            option=option,
            expected_type=exp_type_str,
            current_type=type(config_option).__name__,
        ))

db.create_tables(BaseModel.__subclasses__())

bot = telebot.TeleBot(config.API_TOKEN)
telebot.logger.setLevel(logging.INFO)

for handler_path in config.HANDLERS:
    handler = locate(handler_path)
    if not handler:
        raise ImportError(f"Nothing to import by path {handler_path}")
    handler(bot)

def main():
    bot.polling()

if __name__ == "__main__":
    main()
