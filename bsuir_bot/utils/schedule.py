import datetime
import json

import requests

from bsuir_bot import config


class Lesson:
    TYPE_LECTURE = 1
    TYPE_PRACTICAL = 2
    TYPE_LAB = 3

    TYPE_NAMES = {
        TYPE_LECTURE: "ЛК",
        TYPE_PRACTICAL: "ПЗ",
        TYPE_LAB: "ЛР",
    }

    def __init__(self, subject, lesson_type, group, subgroup, teacher, auditory, start_time, end_time, note):
        self.subject = subject
        self.lesson_type = lesson_type
        self.group = group
        self.subgroup = subgroup
        self.teacher = teacher
        self.auditory = auditory
        self.start_time = start_time
        self.end_time = end_time
        self.note = note

    @classmethod
    def from_info(cls, lesson_info):
        if not isinstance(lesson_info, dict):
            raise TypeError("lesson_info have to be dict, not {}".format(type(lesson_info).__name__))

        lesson_type = cls.resolve_type(lesson_info["lessonType"])
        if not lesson_type:
            return

        teacher = ""
        if len(lesson_info["employee"]):
            teacher = lesson_info["employee"][0]["lastName"]
            teacher += " {}.".format(lesson_info["employee"][0]["firstName"][0])
            teacher += " {}.".format(lesson_info["employee"][0]["middleName"][0])

        auditory = ""
        if len(lesson_info["auditory"]):
            auditory = lesson_info["auditory"][0]

        lesson = Lesson(
            subject=lesson_info["subject"],
            lesson_type=lesson_type,
            group=lesson_info["studentGroup"],
            subgroup=int(lesson_info["numSubgroup"]),
            teacher=teacher,
            auditory=auditory,
            start_time=lesson_info["startLessonTime"],
            end_time=lesson_info["endLessonTime"],
            note=lesson_info["note"],
        )
        return lesson

    @classmethod
    def resolve_type(cls, type_name):
        for tid, tname in cls.TYPE_NAMES.items():
            if tname == type_name:
                return tid
        return None

    def __str__(self):
        return "Lesson {} {}".format(self.subject, self.TYPE_NAMES[self.lesson_type])

    def __repr__(self):
        return "<{}>".format(self.__str__())


class Schedule:
    def __init__(self, group, day=datetime.date.today(), network_timeout=10):
        self.group = group
        self.day = day
        self.network_timeout = network_timeout
        self.lessons = list()
        self.update()

    def load_schedule(self):
        r = requests.get(
            "https://journal.bsuir.by/api/v1/studentGroup/schedule?studentGroup={}".format(self.group),
            timeout=self.network_timeout,
        )
        if r.status_code != 200 or not r.text:
            return False
        return r.json()

    def update(self):
        data = self.load_schedule()
        if not data:
            return False

        self.lessons.clear()

        weekday = self.day.weekday()
        if weekday == 6:
            return True

        weeknum = self.calc_weeknum(self.day)
        schedule = data["schedules"][weekday]["schedule"]
        for lesson_info in schedule:
            if weeknum not in lesson_info["weekNumber"]:
                continue

            lesson = Lesson.from_info(lesson_info)
            if not lesson:
                return False
            self.lessons.append(lesson)
        return True

    @staticmethod
    def calc_weeknum(target_day):
        year = target_day.year
        if target_day.month < 9:
            year -= 1
        first_sept_weeknum = datetime.date(year, 9, 1).isocalendar()[1]
        target_day_weeknum = target_day.isocalendar()[1]
        weeks_num_diff = target_day_weeknum - first_sept_weeknum
        weeknum = weeks_num_diff % 4 + 1
        return weeknum

    def __str__(self):
        return "Schedule {}".format(self.group)

    def __repr__(self):
        return "<{}>".format(self.__str__())
