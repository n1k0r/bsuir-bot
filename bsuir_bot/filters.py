class Filter:
    def __init__(self):
        pass

    def check(self, msg):
        raise NotImplementedError

    def __invert__(self):
        return NotFilter(self)

    def __and__(self, other):
        if isinstance(self, ANDFilter):
            self.filters.append(other)
            return self

        if isinstance(other, ANDFilter):
            other.filters.insert(0, self)
            return other

        and_filter = ANDFilter(self, other)
        return and_filter

    def __or__(self, other):
        if isinstance(self, ORFilter):
            self.filters.append(other)
            return self

        if isinstance(other, ORFilter):
            other.filters.insert(0, self)
            return other

        or_filter = ORFilter(self, other)
        return or_filter

    def __xor__(self, other):
        xor_filter = XORFilter(self, other)
        return xor_filter


class TrueFilter(Filter):
    def check(self, msg):
        return True


class FalseFilter(Filter):
    def check(self, msg):
        return False


class ANDFilter(Filter):
    def __init__(self, *args):
        self.filters = list(args)

    def check(self, msg):
        for filter in self.filters:
            if not filter.check(msg):
                return False
        return True


class ORFilter(Filter):
    def __init__(self, *args):
        self.filters = list(args)

    def check(self, msg):
        for filter in self.filters:
            if filter.check(msg):
                return True
        return False


class XORFilter(Filter):
    def __init__(self, first, second):
        self.first = first
        self.second = second

    def check(self, msg):
        first_result = self.first.check(msg)
        second_result = self.second.check(msg)
        return first_result ^ second_result


class NotFilter(Filter):
    def __init__(self, filter):
        self.filter = filter

    def check(self, msg):
        return not self.filter.check(msg)


class TextFilter(Filter):
    def check(self, msg):
        return bool(msg.text)


class EqualFilter(Filter):
    def __init__(self, text):
        self.text = text

    def check(self, msg):
        return self.text == msg.text


class SubstrFilter(Filter):
    def __init__(self, substr):
        self.substr = substr

    def check(self, msg):
        return self.substr in msg.text.lower()


class ForwardFilter(Filter):
    def check(self, msg):
        return bool(getattr(msg, "forward_from", None))


class PMFilter(Filter):
    def check(self, msg):
        return msg.chat.type == "private"
