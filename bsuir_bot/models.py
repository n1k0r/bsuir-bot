import peewee

from .database import db


class BaseModel(peewee.Model):
    class Meta:
        database = db


class Queue(BaseModel):
    chat_id = peewee.IntegerField()
    name = peewee.CharField()
    is_open = peewee.BooleanField(default=True)
    msg_id = peewee.IntegerField(null=True)


class Participant(BaseModel):
    queue = peewee.ForeignKeyField(Queue, backref="participants", on_delete="CASCADE")
    position = peewee.SmallIntegerField(null=True)
    user_id = peewee.IntegerField()
    first_name = peewee.CharField()
    last_name = peewee.CharField()
    username = peewee.CharField()


class SwapRequest(BaseModel):
    queue = peewee.ForeignKeyField(Queue, backref="swap_requests", on_delete="CASCADE")
    sender = peewee.ForeignKeyField(Participant, backref="swap_requests", on_delete="CASCADE")
    position = peewee.SmallIntegerField()
