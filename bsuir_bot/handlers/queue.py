import random
import re
import traceback

import telebot

from bsuir_bot import config
from bsuir_bot.filters import *
from bsuir_bot.models import Participant, Queue, SwapRequest

from .base import CallbackHandler, MessageHandler


class QueueAction(MessageHandler):
    filters = [
        TextFilter(),
        ~ForwardFilter(),
        PMFilter() | SubstrFilter(config.BOT_NAME.lower()),
        ~PMFilter(),
        SubstrFilter("лаб"),
    ]

    MSG_NEW_QUEUE = "Очередь на _{name}_\nСдает *{count}* человек"
    MSG_CLOSED_QUEUE = "Очередь на _{name}_\nРегистрация завершена."

    BOT_NAME_RE = re.compile(config.BOT_NAME, re.IGNORECASE)

    def action(self, message):
        cid = message.chat.id

        is_admin = False
        admins = [admin.user.id for admin in self.bot.get_chat_administrators(cid)]
        admins += config.PRIVILEGED_USERS
        for admin in admins:
            if admin == message.from_user.id:
                is_admin = True
                break
        if not is_admin:
            return

        name = self.BOT_NAME_RE.sub("", message.text).strip()
        queue = Queue.create(chat_id=cid, name=name)

        msg = self.MSG_NEW_QUEUE.format(count=0, name=name) + "."
        result = self.bot.send_message(
            message.chat.id,
            msg,
            parse_mode="Markdown",
            reply_markup=self.get_requests_markup(queue),
        )

        queue.msg_id = result.message_id
        queue.save()

    @staticmethod
    def get_requests_markup(queue):
        markup = telebot.types.InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(
            telebot.types.InlineKeyboardButton("Участвовать ☑️", callback_data=f"request_register|{queue.id}"),
            telebot.types.InlineKeyboardButton("Выйти 🚪", callback_data=f"request_unregister|{queue.id}"),
            telebot.types.InlineKeyboardButton("Закончить регистрацию 🎲", callback_data=f"request_random|{queue.id}"),
        )
        return markup

    @classmethod
    def update_register_message(cls, bot, queue, final=False):
        msg = cls.generate_register_message(queue, final)

        try:
            bot.edit_message_text(
                chat_id=queue.chat_id,
                message_id=queue.msg_id,
                text=msg,
                parse_mode="Markdown",
                reply_markup=cls.get_requests_markup(queue) if not final else None,
            )
        except telebot.apihelper.ApiException:
            print("exception")
            traceback.print_exc()

    @classmethod
    def generate_register_message(cls, queue, final=False):
        count = queue.participants.select().count()

        if not final:
            msg = cls.MSG_NEW_QUEUE.format(count=count, name=queue.name) + ("." if not count else "")
        else:
            msg = cls.MSG_CLOSED_QUEUE.format(name=queue.name)

        if not count or final:
            return msg

        msg += ":\n"

        users = queue.participants.select()

        for user in users:
            name = user.first_name
            last_name = user.last_name
            if last_name:
                name += " " + last_name

            msg += "[{name}](tg://user?id={id})\n".format(
                id=user.user_id,
                name=name,
            )

        return msg


class QueueRegisterCallback(CallbackHandler):
    filters = [
        lambda call: call.data.startswith("request_register")
    ]

    def action(self, call):
        qid = int(call.data.split("|")[1])
        queue = Queue.get_or_none(qid)

        if queue is None or not queue.is_open or queue.chat_id != call.message.chat.id:
            self.bot.answer_callback_query(call.id, "Очередь закрыта", True)
            return

        if Participant.get_or_none(
            Participant.user_id == call.from_user.id,
            Participant.queue == queue,
        ) is not None:
            self.bot.answer_callback_query(call.id, "Уже в очереди", True)
            return

        Participant.create(
            queue=queue,
            user_id=call.from_user.id,
            first_name=call.from_user.first_name or "",
            last_name=call.from_user.last_name or "",
            username=call.from_user.username or "",
        )

        self.bot.answer_callback_query(call.id, "Добавил в очередь", True)

        QueueAction.update_register_message(self.bot, queue)


class QueueUnregisterCallback(CallbackHandler):
    filters = [
        lambda call: call.data.startswith("request_unregister")
    ]

    def action(self, call):
        qid = int(call.data.split("|")[1])
        queue = Queue.get_or_none(qid)

        if queue is None or not queue.is_open or queue.chat_id != call.message.chat.id:
            self.bot.answer_callback_query(call.id, "Очередь закрыта", True)
            return

        participant = Participant.get_or_none(
            Participant.user_id == call.from_user.id,
            Participant.queue == queue,
        )
        if participant is None:
            self.bot.answer_callback_query(call.id, "Не был в очереди", True)
            return

        participant.delete_instance()

        self.bot.answer_callback_query(call.id, "Удалил из очереди", True)

        QueueAction.update_register_message(self.bot, queue)


class QueueRandomCallback(CallbackHandler):
    filters = [
        lambda call: call.data.startswith("request_random")
    ]

    def action(self, call):
        cid = call.message.chat.id

        is_admin = False
        admins = [admin.user.id for admin in self.bot.get_chat_administrators(cid)]
        admins += config.PRIVILEGED_USERS
        for admin in admins:
            if admin == call.from_user.id:
                is_admin = True
                break
        if not is_admin:
            self.bot.answer_callback_query(call.id, "Уходи!", True)
            return

        qid = int(call.data.split("|")[1])
        queue = Queue.get_or_none(qid)

        if queue is None or not queue.is_open or queue.chat_id != call.message.chat.id:
            self.bot.answer_callback_query(call.id, "Ошибка: очередь уже закрыта!", True)
            return

        users = list(queue.participants.select())
        random.shuffle(users)

        queue.is_open = False
        for pos, user in enumerate(users):
            user.position = pos + 1
            user.save()

        count = len(users)
        msg = self.generate_queue_message(queue)
        if count:
            msg += "\nЧтобы поменяться местами, выберите желаемые позиции:"
        result = self.bot.send_message(
            cid,
            msg,
            parse_mode="Markdown",
            reply_markup=self.get_requests_markup(queue),
        )


        QueueAction.update_register_message(self.bot, queue, True)

        queue.msg_id = result.message_id
        queue.save()

    @classmethod
    def update_queue_message(cls, bot, queue, final=False):
        count = queue.participants.select().count()
        msg = cls.generate_queue_message(queue)
        if count and not final:
            msg += "\nЧтобы поменяться местами, выберите желаемые позиции:"

        qmid = queue.msg_id
        try:
            bot.edit_message_text(
                chat_id=queue.chat_id,
                message_id=qmid,
                text=msg,
                parse_mode="Markdown",
                reply_markup=cls.get_requests_markup(queue) if not final else None,
            )
        except telebot.apihelper.ApiException:
            traceback.print_exc()

    @staticmethod
    def generate_queue_message(queue):
        users = queue.participants.select()
        users = sorted(users, key=lambda user: user.position)

        result = f"Лабу _{queue.name}_ сдаём в таком порядке:\n"
        if not users:
            result += "Нет желающих"
        for user in users:
            name = user.first_name
            last_name = user.last_name
            if last_name:
                name += " " + last_name
            user_str = "{pos}. [{name}](tg://user?id={id})".format(
                pos=user.position,
                id=user.user_id,
                name=name,
            )

            desired_pos = [
                req.position for req in
                SwapRequest.select(SwapRequest.position).where(
                    SwapRequest.queue == queue,
                    SwapRequest.sender == user,
                )
            ]
            if desired_pos:
                desired_pos.sort()
                pos_list = ", ".join([str(n) for n in desired_pos])
                user_str += " (обмен на {})".format(pos_list)
            result += "{}\n".format(user_str)

        return result

    @staticmethod
    def get_requests_markup(queue):
        count = queue.participants.select().count()
        markup = telebot.types.InlineKeyboardMarkup()
        markup.row_width = 7
        markup.add(
            *[
                telebot.types.InlineKeyboardButton(
                    str(i),
                    callback_data=f"request_swap|{i}|{queue.id}",
                )
                for i in range(1, count+1)
            ],
            telebot.types.InlineKeyboardButton(
                "☑️",
                callback_data=f"request_swap|-1|{queue.id}",
            ),
            telebot.types.InlineKeyboardButton(
                "🚪",
                callback_data=f"request_swap|-2|{queue.id}",
            ),
            telebot.types.InlineKeyboardButton(
                "🔒",
                callback_data=f"request_swap|0|{queue.id}",
            ),
        )
        return markup


class QueueSwapCallback(CallbackHandler):
    filters = [
        lambda call: call.data.startswith("request_swap")
    ]

    def action(self, call):
        request, request_pos, qid = call.data.split("|")
        request_pos, qid = int(request_pos), int(qid)
        uid = call.from_user.id

        queue = Queue.get_or_none(qid)
        if queue is None or queue.is_open or queue.chat_id != call.message.chat.id:
            self.bot.answer_callback_query(call.id, "Очередь более не актуальна", True)
            return

        if request_pos == 0:
            is_admin = False
            admins = [admin.user.id for admin in self.bot.get_chat_administrators(call.message.chat.id)]
            admins += config.PRIVILEGED_USERS
            for admin in admins:
                if admin == uid:
                    is_admin = True
                    break
            if not is_admin:
                self.bot.answer_callback_query(call.id, "Уходи!", True)
                return
            QueueRandomCallback.update_queue_message(self.bot, queue, True)

            queue.delete_instance()
            self.bot.answer_callback_query(call.id)
            return
        elif request_pos == -1:
            if Participant.get_or_none(
                Participant.user_id == call.from_user.id,
                Participant.queue == queue,
            ) is not None:
                self.bot.answer_callback_query(call.id, "Уже в очереди", True)
                return

            count = queue.participants.count()
            Participant.create(
                queue=queue,
                position=count+1,
                user_id=call.from_user.id,
                first_name=call.from_user.first_name or "",
                last_name=call.from_user.last_name or "",
                username=call.from_user.username or "",
            )

            QueueRandomCallback.update_queue_message(self.bot, queue)
            self.bot.answer_callback_query(call.id)
            return
        elif request_pos == -2:
            participant = Participant.get_or_none(
                Participant.user_id == call.from_user.id,
                Participant.queue == queue,
            )
            if participant is None:
                self.bot.answer_callback_query(call.id, "Не был в очереди", True)
                return

            count = queue.participants.count()
            if participant.position == count:
                SwapRequest.delete().where(
                    SwapRequest.queue == queue,
                    SwapRequest.position == participant.position,
                ).execute()

            Participant.update(
                position = Participant.position - 1,
            ).where(
                Participant.queue == queue,
                Participant.position > participant.position,
            ).execute()

            participant.delete_instance()

            QueueRandomCallback.update_queue_message(self.bot, queue)
            self.bot.answer_callback_query(call.id)
            return

        participant = Participant.get_or_none(
            Participant.queue == queue,
            Participant.user_id == uid,
        )
        if participant is None:
            self.bot.answer_callback_query(call.id, "Нельзя поменяться без регистрации в очереди", True)
            return

        requser = Participant.get_or_none(
            Participant.queue == queue,
            Participant.position == request_pos,
        )
        if requser is None:
            self.bot.answer_callback_query(call.id, "Позиция отсутствует", True)
            return
        if uid == requser.user_id:
            self.bot.answer_callback_query(call.id, "Поменяться с собой? Ну ладно...", True)
            return
        requser_full_name = requser.first_name
        if requser.last_name: requser_full_name += " " + requser.last_name

        requser_pos = SwapRequest.get_or_none(
            SwapRequest.queue == queue,
            SwapRequest.sender == requser,
            SwapRequest.position == participant.position,
        )
        if requser_pos is None:
            request, created = SwapRequest.get_or_create(
                queue=queue,
                sender=participant,
                position=request_pos,
            )

            msg = ""
            if created:
                msg = "Оставлена заявка на обмен позицией с {full_name}. Если {first_name} выберет вашу позицию, то вы поменяетесь местами.\nЧтобы отозвать заявку нажмите по этой позиции повторно.".format(
                    first_name=requser.first_name,
                    full_name=requser_full_name,
                )
            else:
                request.delete_instance()
                msg = "Заявка на обмен отозвана"
            QueueRandomCallback.update_queue_message(self.bot, queue)
            self.bot.answer_callback_query(call.id, msg, True)
            return

        participant.position, requser.position = requser.position, participant.position
        participant.save()
        requser.save()
        SwapRequest.delete().where(
            SwapRequest.queue == queue,
            SwapRequest.sender.in_([participant, requser])
        ).execute()

        QueueRandomCallback.update_queue_message(self.bot, queue)

        msg = "Вы поменялись в очереди с {full_name}".format(
            full_name=requser_full_name,
        )
        self.bot.answer_callback_query(call.id, msg, True)
