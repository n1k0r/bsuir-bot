import types

from bsuir_bot.filters import Filter


class Handler:
    def __init__(self, bot):
        self.bot = bot
        if not getattr(self, "filters", None):
            self.filters = []

    def filter(self, data):
        for filter in self.filters:
            result = False
            if isinstance(filter, (types.FunctionType, types.LambdaType, types.MethodType)):
                result = filter(data)
            elif isinstance(filter, (staticmethod, classmethod)):
                result = filter.__func__(data)
            elif isinstance(filter, Filter):
                result = filter.check(data)
            else:
                raise TypeError("filter have to be type of Filter or function")

            if not result:
                return False

        return True

    def action(self, data):
        raise NotImplementedError


class MessageHandler(Handler):
    def __init__(self, bot):
        super().__init__(bot)

        handler = bot.message_handler(func=self.filter)
        handler(self.action)  # register handler


class CallbackHandler(Handler):
    def __init__(self, bot):
        super().__init__(bot)

        handler = bot.callback_query_handler(func=self.filter)
        handler(self.action)
