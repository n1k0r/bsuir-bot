import random

from bsuir_bot import config
from bsuir_bot.filters import *

from .base import CallbackHandler, MessageHandler


class UnknownAction(MessageHandler):
    filters = [
        TextFilter(),
        ~ForwardFilter(),
        PMFilter() | SubstrFilter(config.BOT_NAME.lower()),
    ]

    def action(self, message):
        self.bot.reply_to(message, "Меня звали? 🤖")


class AdviceBookAction(MessageHandler):
    filters = [
        TextFilter(),
        ~ForwardFilter(),
        PMFilter() | SubstrFilter(config.BOT_NAME.lower()),
        SubstrFilter("чита"),
    ]

    def action(self, message):
        book = random.choice(config.BOOKS)
        book_msg = "📖 Советую прочитать: [{text}]({link})".format(
            text=book["text"],
            link=book["link"],
        )
        self.bot.send_message(message.chat.id, book_msg, parse_mode="Markdown")


class QuestionAction(MessageHandler):
    filters = [
        TextFilter(),
        ~ForwardFilter(),
        PMFilter() | SubstrFilter(config.BOT_NAME.lower()),
        SubstrFilter("?"),
    ]

    def action(self, message):
        self.bot.send_message(message.chat.id, random.choice(["Да", "Нет", "Может быть"]))
