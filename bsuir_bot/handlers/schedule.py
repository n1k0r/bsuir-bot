import calendar
import datetime

from bsuir_bot import config
from bsuir_bot.filters import *
from bsuir_bot.utils import schedule

from .base import CallbackHandler, MessageHandler

WEEK_DAYS = ["понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"]
WEEK_DAYS_NAMED = ["понедельник", "вторник", "среду", "четверг", "пятницу", "субботу", "воскресенье"]
MONTHES = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"]


class ScheduleAction(MessageHandler):
    filters = [
        TextFilter(),
        ~ForwardFilter(),
        PMFilter() | SubstrFilter(config.BOT_NAME.lower()),
        SubstrFilter("расписание"),
    ]

    @staticmethod
    def parse_day(msg_text):
        day = None

        if not day:
            if " послезавтра" in msg_text:
                day = datetime.date.today() + datetime.timedelta(days=2)

        if not day:
            if "завтра" in msg_text:
                day = datetime.date.today() + datetime.timedelta(days=1)

        if not day:
            for i, week_day in enumerate(WEEK_DAYS):
                if week_day in msg_text:
                    day = datetime.date.today()
                    while day.weekday() != i:
                        day += datetime.timedelta(days=1)
                    break

        if not day:
            for i, week_day in enumerate(WEEK_DAYS_NAMED):
                if week_day in msg_text:
                    day = datetime.date.today()
                    while day.weekday() != i:
                        day += datetime.timedelta(days=1)
                    break

        if not day:
            year = datetime.date.today().year
            daynum = None
            for part in msg_text.split(" "):
                if part.isnumeric() and int(part) >= 1 and int(part) <= 31:
                    daynum = int(part)
            if daynum:
                for i, monthstr in enumerate(MONTHES):
                    if monthstr in msg_text:
                        first_day, max_day = calendar.monthrange(year, i + 1)
                        if daynum > max_day:
                            break
                        day = datetime.date(year, i + 1, daynum)
                        break

        if not day:
            day = datetime.date.today()

        return day

    def action(self, message):
        day = self.parse_day(message.text)
        if not day:
            self.bot.send_message(message.chat.id, "Не понял")
            return

        day_schedule = schedule.Schedule(config.STUDENT_GROUP, day)

        date_str = "{daynum} {monthstr} ({daystr})".format(
            daystr=WEEK_DAYS[day.weekday()],
            daynum=day.day,
            monthstr=MONTHES[day.month - 1],
        )
        result = "Расписание на {date} для группы {group}\n".format(
            date=date_str,
            group=config.STUDENT_GROUP,
        )

        if len(day_schedule.lessons) is 0:
            result += "В этот день нет пар! 🎉"
        else:
            for lesson in day_schedule.lessons:
                time = lesson.start_time + "-" + lesson.end_time

                subgroup = "\*⃣"
                if lesson.subgroup:
                    subgroup = "1⃣" if lesson.subgroup == 1 else "2️⃣"

                lesson_type = {
                    schedule.Lesson.TYPE_LECTURE: "📢",
                    schedule.Lesson.TYPE_PRACTICAL: "📝",
                    schedule.Lesson.TYPE_LAB: "🔬️",
                }[lesson.lesson_type]

                if lesson.subject == "ФизК":
                    format_string = "{time} \*⃣ 🏃🚴🏊🏻⛹🏻️‍♀️🤸🏼‍♂️🏋🏼 {note}\n"
                else:
                    format_string = "{time} {subgroup} {lesson_type} {auditory} {subject} {note}\n"

                result += format_string.format(
                    time=time,
                    subgroup=subgroup,
                    lesson_type=lesson_type,
                    subject=lesson.subject,
                    auditory=lesson.auditory,
                    note="\n_{}_".format(lesson.note) if lesson.note else "",
                )

        self.bot.send_message(message.chat.id, result, parse_mode="Markdown")
