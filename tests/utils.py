import json

from telebot.types import Message


def test_msg(text=None, msg_id=123, date=1535760000, from_id=456, from_bot=False, from_name="Tester", chat_id=789, chat_type="group", chat_title="Dev chat", **kwargs):
    data = {
        "message_id": msg_id,
        "from": {
            "id": from_id,
            "is_bot": from_bot,
            "first_name": from_name,
        },
        "date": date,
        "chat": {
            "id": chat_id,
            "type": chat_type,
            "title": chat_title,
        },
        "text": text,
    }
    for key, value in kwargs.items():
        data[key] = value

    msg = Message.de_json(json.dumps(data))
    return msg
