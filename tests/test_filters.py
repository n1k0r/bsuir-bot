import unittest

from bsuir_bot import filters
from .utils import test_msg


class TestFilterInvert(unittest.TestCase):
    def setUp(self):
        self.msg = test_msg()

    def test_not_true(self):
        tf = filters.TrueFilter()
        nottf = ~tf
        self.assertFalse(nottf.check(self.msg))

    def test_not_false(self):
        ff = filters.FalseFilter()
        notff = ~ff
        self.assertTrue(notff.check(self.msg))


class TestFilterAnd(unittest.TestCase):
    def setUp(self):
        self.msg = test_msg()

    def test_true_and_true(self):
        filter = filters.TrueFilter() & filters.TrueFilter()
        self.assertTrue(filter.check(self.msg))

    def test_true_and_false(self):
        filter = filters.TrueFilter() & filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))

    def test_false_and_true(self):
        filter = filters.FalseFilter() & filters.TrueFilter()
        self.assertFalse(filter.check(self.msg))

    def test_false_and_false(self):
        filter = filters.FalseFilter() & filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))

    def test_all_true(self):
        filter = filters.TrueFilter() & filters.TrueFilter() & filters.TrueFilter()
        self.assertTrue(filter.check(self.msg))

    def test_one_true(self):
        filter = filters.TrueFilter() & filters.FalseFilter() & filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))

    def test_all_false(self):
        filter = filters.FalseFilter() & filters.FalseFilter() & filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))


class TestFilterOr(unittest.TestCase):
    def setUp(self):
        self.msg = test_msg()

    def test_true_or_true(self):
        filter = filters.TrueFilter() | filters.TrueFilter()
        self.assertTrue(filter.check(self.msg))

    def test_true_or_false(self):
        filter = filters.TrueFilter() | filters.FalseFilter()
        self.assertTrue(filter.check(self.msg))

    def test_false_or_true(self):
        filter = filters.FalseFilter() | filters.TrueFilter()
        self.assertTrue(filter.check(self.msg))

    def test_false_or_false(self):
        filter = filters.FalseFilter() | filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))

    def test_all_true(self):
        filter = filters.TrueFilter() | filters.TrueFilter() | filters.TrueFilter()
        self.assertTrue(filter.check(self.msg))

    def test_one_true(self):
        filter = filters.TrueFilter() | filters.FalseFilter() | filters.FalseFilter()
        self.assertTrue(filter.check(self.msg))

    def test_all_false(self):
        filter = filters.FalseFilter() | filters.FalseFilter() | filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))


class TestFilterXor(unittest.TestCase):
    def setUp(self):
        self.msg = test_msg()

    def test_true_xor_true(self):
        filter = filters.TrueFilter() ^ filters.TrueFilter()
        self.assertFalse(filter.check(self.msg))

    def test_true_xor_false(self):
        filter = filters.TrueFilter() ^ filters.FalseFilter()
        self.assertTrue(filter.check(self.msg))

    def test_false_xor_true(self):
        filter = filters.FalseFilter() ^ filters.TrueFilter()
        self.assertTrue(filter.check(self.msg))

    def test_false_xor_false(self):
        filter = filters.FalseFilter() ^ filters.FalseFilter()
        self.assertFalse(filter.check(self.msg))


class TestTrueFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.TrueFilter()

    def test_empty(self):
        msg = test_msg()
        self.assertTrue(self.filter.check(msg))

    def test_string(self):
        msg = test_msg(text="test message")
        self.assertTrue(self.filter.check(msg))


class TestFalseFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.FalseFilter()

    def test_empty(self):
        msg = test_msg()
        self.assertFalse(self.filter.check(msg))

    def test_string(self):
        msg = test_msg(text="test message")
        self.assertFalse(self.filter.check(msg))


class TestNotFilter(unittest.TestCase):
    def test_true(self):
        true_filter = filters.TrueFilter()
        filter = filters.NotFilter(true_filter)
        msg = test_msg()
        self.assertFalse(filter.check(msg))

    def test_false(self):
        false_filter = filters.FalseFilter()
        filter = filters.NotFilter(false_filter)
        msg = test_msg()
        self.assertTrue(filter.check(msg))

    def test_double_not_true(self):
        true_filter = filters.TrueFilter()
        filter = filters.NotFilter(filters.NotFilter(true_filter))
        msg = test_msg()
        self.assertTrue(filter.check(msg))

    def test_double_not_false(self):
        false_filter = filters.FalseFilter()
        filter = filters.NotFilter(filters.NotFilter(false_filter))
        msg = test_msg()
        self.assertFalse(filter.check(msg))


class TestANDFilter(unittest.TestCase):
    def test_all_true(self):
        filter = filters.ANDFilter(
            filters.TrueFilter(),
            filters.TrueFilter(),
            filters.TrueFilter(),
        )
        msg = test_msg()
        self.assertTrue(filter.check(msg))

    def test_all_false(self):
        filter = filters.ANDFilter(
            filters.FalseFilter(),
            filters.FalseFilter(),
            filters.FalseFilter(),
        )
        msg = test_msg()
        self.assertFalse(filter.check(msg))

    def test_one_true(self):
        filter = filters.ANDFilter(
            filters.TrueFilter(),
            filters.FalseFilter(),
            filters.FalseFilter(),
        )
        msg = test_msg()
        self.assertFalse(filter.check(msg))


class TestORFilter(unittest.TestCase):
    def test_all_true(self):
        filter = filters.ORFilter(
            filters.TrueFilter(),
            filters.TrueFilter(),
            filters.TrueFilter(),
        )
        msg = test_msg()
        self.assertTrue(filter.check(msg))

    def test_all_false(self):
        filter = filters.ORFilter(
            filters.FalseFilter(),
            filters.FalseFilter(),
            filters.FalseFilter(),
        )
        msg = test_msg()
        self.assertFalse(filter.check(msg))

    def test_one_true(self):
        filter = filters.ORFilter(
            filters.TrueFilter(),
            filters.FalseFilter(),
            filters.FalseFilter(),
        )
        msg = test_msg()
        self.assertTrue(filter.check(msg))


class TestXORFilter(unittest.TestCase):
    def setUp(self):
        self.msg = test_msg()

    def test_both_true(self):
        filter = filters.XORFilter(
            filters.TrueFilter(),
            filters.TrueFilter(),
        )
        self.assertFalse(filter.check(self.msg))

    def test_both_false(self):
        filter = filters.XORFilter(
            filters.FalseFilter(),
            filters.FalseFilter(),
        )
        self.assertFalse(filter.check(self.msg))

    def test_one_true(self):
        filter = filters.XORFilter(
            filters.TrueFilter(),
            filters.FalseFilter(),
        )
        self.assertTrue(filter.check(self.msg))


class TestTextFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.TextFilter()

    def test_text(self):
        msg = test_msg(text="some message")
        self.assertTrue(self.filter.check(msg))

    def test_none(self):
        msg = test_msg(text=None)
        self.assertFalse(self.filter.check(msg))

    def test_empty(self):
        msg = test_msg(text="")
        self.assertFalse(self.filter.check(msg))


class TestEqualFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.EqualFilter("laptop")

    def test_equal(self):
        msg = test_msg(text="laptop")
        self.assertTrue(self.filter.check(msg))

    def test_contain(self):
        msg = test_msg(text="i have laptop")
        self.assertFalse(self.filter.check(msg))

    def test_notequal(self):
        msg = test_msg(text="i have only desktop computer")
        self.assertFalse(self.filter.check(msg))


class TestSubstrFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.SubstrFilter("word")

    def test_contain(self):
        msg = test_msg(text="this string contain word")
        self.assertTrue(self.filter.check(msg))

    def test_notcontain(self):
        msg = test_msg(text="this string doesn't contain")
        self.assertFalse(self.filter.check(msg))


class TestForwardFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.ForwardFilter()

    def test_forward(self):
        msg = test_msg(
            text="some text",
            forward_from={
                "id": 358,
                "is_bot": False,
                "first_name": "Author",
            }
        )
        self.assertTrue(self.filter.check(msg))

    def test_notforward(self):
        msg = test_msg(text="some text")
        self.assertFalse(self.filter.check(msg))


class TestPMFilter(unittest.TestCase):
    def setUp(self):
        self.filter = filters.PMFilter()

    def test_pm(self):
        msg = test_msg(chat_type="private")
        self.assertTrue(self.filter.check(msg))

    def test_other(self):
        msg = test_msg(chat_type="group")
        self.assertFalse(self.filter.check(msg))
