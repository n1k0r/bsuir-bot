FROM python:3.7.0-alpine3.8

WORKDIR /app

COPY requirements.txt .
RUN pip3 install -r requirements.txt \
    && rm -r /root/.cache/pip

COPY . .

CMD ["python3", "-m", "bsuir_bot"]
